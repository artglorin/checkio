# Say Hi

In this mission you should write a function that introduce a person with a given parameters in attributes.

__Input:__ Two arguments. String and positive integer.

__Output:__ String.

__Example:__
```python
say_hi("Alex", 32) == "Hi. My name is Alex and I'm 32 years old"
say_hi("Frank", 68) == "Hi. My name is Frank and I'm 68 years old"

```