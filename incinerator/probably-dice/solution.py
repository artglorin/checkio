from math import factorial


def compute_combination(set_size, combinations_count):
    if combinations_count == 0 or set_size == combinations_count:
        return 1
    else:
        numerator = factorial(set_size)
        denominator = factorial(combinations_count) * factorial(set_size - combinations_count)
        return numerator // denominator


def count_combinations(value, count, target, sides):
    return (-1) ** value\
           * compute_combination(count, value)\
           * compute_combination(target - sides * value - 1, count - 1)


def count_occurrences(number_of_shots, sides, target, max_occurrences):
    return sum(count_combinations(value, number_of_shots, target, sides) for value in range(0, max_occurrences + 1))


def probability(dice_number, sides, target):
    try:
        total_combinations = sides ** dice_number
        one_chance_value = 1 / total_combinations
        return one_chance_value * count_occurrences(dice_number, sides, target, (target - dice_number) // sides)
    except ValueError:
        return 0
