
class Warrior:

    def __init__(self):
        self.health = 50
        self.attack = 5

    @property
    def is_alive(self) -> bool:
        return self.health > 0


class Knight(Warrior):

    def __init__(self):
        super().__init__()
        self.attack = 7


class Army:

    def __init__(self):
        self.warriors = []

    def add_units(self, unit_class: type, amount: int):
        self.warriors += [unit_class() for _ in range(amount)]


class Battle:

    def fight(self, left_army: Army,  right_army: Army):
        while len(left_army.warriors) != 0 and len(right_army.warriors) != 0:
            left_army_warrior = left_army.warriors[0]
            right_army_warrior = right_army.warriors[0]
            if fight(left_army_warrior, right_army_warrior):
                del right_army.warriors[0]
            else:
                del left_army.warriors[0]
        return len(left_army.warriors) != 0


def fight(unit_1: Warrior, unit_2: Warrior) -> bool:
    while unit_1.is_alive and unit_2.is_alive:
        unit_2.health -= unit_1.attack
        unit_1.health -= unit_2.attack if unit_2.is_alive else 0
    return unit_1.is_alive or unit_1.is_alive == unit_2.is_alive


if __name__ == '__main__':
    # These "asserts" using only for self-checking and not necessary for auto-testing

    # fight tests
    chuck = Warrior()
    bruce = Warrior()
    carl = Knight()
    dave = Warrior()
    mark = Warrior()

    assert fight(chuck, bruce) == True
    assert fight(dave, carl) == False
    assert chuck.is_alive == True
    assert bruce.is_alive == False
    assert carl.is_alive == True
    assert dave.is_alive == False
    assert fight(carl, mark) == False
    assert carl.is_alive == False

    # battle tests
    my_army = Army()
    my_army.add_units(Knight, 3)

    enemy_army = Army()
    enemy_army.add_units(Warrior, 3)

    army_3 = Army()
    army_3.add_units(Warrior, 20)
    army_3.add_units(Knight, 5)

    army_4 = Army()
    army_4.add_units(Warrior, 30)

    battle = Battle()

    assert battle.fight(my_army, enemy_army) == True
    assert battle.fight(army_3, army_4) == False
    print("Coding complete? Let's try tests!")
