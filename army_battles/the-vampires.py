class Warrior:

    def __init__(self):
        self.health = 50
        self.attack = 5
        self.defense = 0

    @property
    def is_alive(self) -> bool:
        return self.health > 0


class Defender(Warrior):

    def __init__(self):
        super().__init__()
        self.health = 60
        self.attack = 3
        self.defense = 2


class Knight(Warrior):

    def __init__(self):
        super().__init__()
        self.attack = 7


class Vampire(Warrior):

    def __init__(self):
        super().__init__()
        self.health = 40
        self.attack = 4
        self.vampirism = 50


class Army:

    def __init__(self):
        self.warriors = []

    def add_units(self, unit_class: type, amount: int):
        self.warriors += [unit_class() for _ in range(amount)]


class BattleUtils:
    @staticmethod
    def compute_damage(defender: Warrior, assaulter: Warrior) -> int:
        return abs(min(defender.defense - assaulter.attack, 0)) if assaulter.is_alive else 0

    @staticmethod
    def compute_vampire_heal(unit_1: Warrior, damage: int) -> int:
        return damage / 2 if isinstance(unit_1, Vampire) else 0

    @staticmethod
    def execute_round(unit_1: Warrior, unit_2: Warrior):
        BattleUtils.make_attack(unit_2, unit_1)
        BattleUtils.make_attack(unit_1, unit_2)

    @staticmethod
    def make_attack(defender: Warrior, assaulter: Warrior):
        damage = BattleUtils.compute_damage(defender, assaulter)
        vampire_heal = BattleUtils.compute_vampire_heal(assaulter, damage)
        defender.health -= damage
        assaulter.health += vampire_heal

    @staticmethod
    def duel(unit_1: Warrior, unit_2: Warrior) -> bool:
        while unit_1.is_alive and unit_2.is_alive:
            BattleUtils.execute_round(unit_1, unit_2)
        return unit_1.is_alive or unit_1.is_alive == unit_2.is_alive

    @staticmethod
    def fight(left_army: Army, right_army: Army):
        while len(left_army.warriors) != 0 and len(right_army.warriors) != 0:
            left_army_warrior = left_army.warriors[0]
            right_army_warrior = right_army.warriors[0]
            if fight(left_army_warrior, right_army_warrior):
                del right_army.warriors[0]
            else:
                del left_army.warriors[0]
        return len(left_army.warriors) != 0


class Battle:
    @staticmethod
    def fight(left_army: Army, right_army: Army):
        return BattleUtils.fight(left_army, right_army)


def fight(unit_1: Warrior, unit_2: Warrior) -> bool:
    return BattleUtils.duel(unit_1, unit_2)
