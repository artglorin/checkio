import math


class Warrior:

    def __init__(self):
        self.health = 50
        self.attack = 5
        self.defense = 0

    @property
    def is_alive(self) -> bool:
        return self.health > 0


class Defender(Warrior):

    def __init__(self):
        super().__init__()
        self.health = 60
        self.attack = 3
        self.defense = 2


class Knight(Warrior):

    def __init__(self):
        super().__init__()
        self.attack = 7


class Vampire(Warrior):

    def __init__(self):
        super().__init__()
        self.health = 40
        self.attack = 4
        self.vampirism = 50


class Lancer(Warrior):

    def __init__(self):
        super().__init__()
        self.health = 50
        self.attack = 6


class Army:

    def __init__(self):
        self.warriors = []

    def add_units(self, unit_class: type, amount: int):
        self.warriors += [unit_class() for _ in range(amount)]


class BattleUtils:
    @staticmethod
    def compute_damage(defender: Warrior, assaulter: Warrior) -> int:
        return abs(min(defender.defense - assaulter.attack, 0)) if assaulter.is_alive else 0

    @staticmethod
    def compute_vampire_heal(unit_1: Warrior, damage: int) -> int:
        return damage / 2 if isinstance(unit_1, Vampire) else 0

    @staticmethod
    def execute_round(unit_1: Warrior, unit_2: Warrior):
        BattleUtils.make_attack(unit_2, unit_1)
        BattleUtils.make_attack(unit_1, unit_2)

    @staticmethod
    def make_attack(defender: Warrior, assaulter: Warrior, attack_strength: float = 1):
        damage = math.ceil(BattleUtils.compute_damage(defender, assaulter) * attack_strength)
        vampire_heal = BattleUtils.compute_vampire_heal(assaulter, damage)
        defender.health -= damage
        assaulter.health += vampire_heal

    @staticmethod
    def duel(unit_1: Warrior, unit_2: Warrior) -> bool:
        while unit_1.is_alive and unit_2.is_alive:
            BattleUtils.execute_round(unit_1, unit_2)
        return unit_1.is_alive or unit_1.is_alive == unit_2.is_alive

    @staticmethod
    def create_team(warriors: list, team_size=1) -> list:
        result = []
        warrior_index = 0
        for _ in range(team_size):
            warrior_index = BattleUtils.get_next_alive_warrior_index(warriors, warrior_index)
            if warrior_index < 0:
                return result
            result.append(warriors[warrior_index])
            warrior_index += 1
        return result

    @staticmethod
    def team_battle(left_band: list, right_band: list):
        left_index = BattleUtils.get_next_alive_warrior_index(left_band)
        right_index = BattleUtils.get_next_alive_warrior_index(right_band)
        while left_index >= 0 and right_index >= 0:
            unit_1 = BattleUtils.get_warrior(left_band, left_index)
            unit_2 = BattleUtils.get_warrior(right_band, right_index)
            BattleUtils.execute_round(unit_1, unit_2)
            left_index = BattleUtils.get_next_alive_warrior_index(left_band)
            right_index = BattleUtils.get_next_alive_warrior_index(right_band)
            if right_index > 0 and isinstance(unit_1, Lancer):
                BattleUtils.make_attack(BattleUtils.get_warrior(right_band, right_index), unit_1, 0.5)
                right_index = BattleUtils.get_next_alive_warrior_index(right_band, right_index)
            if left_index > 0 and isinstance(unit_2, Lancer):
                BattleUtils.make_attack(BattleUtils.get_warrior(left_band, left_index), unit_2, 0.5)
                left_index = BattleUtils.get_next_alive_warrior_index(left_band, left_index)

    @staticmethod
    def get_next_alive_warrior_index(warriors: list, index_from: int = 0) -> int:
        for index, warrior in enumerate(warriors[index_from::]):
            if warrior.is_alive:
                return index + index_from
        return -1

    @staticmethod
    def get_warrior(warriors: list, index: int) -> Warrior or None:
        if index >= 0:
            return warriors[index]
        else:
            return None

    @staticmethod
    def fight(left_army: Army, right_army: Army):
        left_army_warriors = left_army.warriors
        right_army_warriors = right_army.warriors
        while len(left_army_warriors) > 0 and len(right_army_warriors) > 0:
            BattleUtils.team_battle(BattleUtils.create_team(left_army_warriors, 2),
                                    BattleUtils.create_team(right_army_warriors, 2))
            left_army_warriors = BattleUtils.get_alive(left_army_warriors)
            right_army_warriors = BattleUtils.get_alive(right_army_warriors)

        return len(BattleUtils.get_alive(left_army_warriors)) != 0

    @staticmethod
    def get_alive(army: list):
        return list(filter(lambda x: x.is_alive, army))


class Battle:
    @staticmethod
    def fight(left_army: Army, right_army: Army):
        return BattleUtils.fight(left_army, right_army)


def fight(unit_1: Warrior, unit_2: Warrior) -> bool:
    return BattleUtils.duel(unit_1, unit_2)
