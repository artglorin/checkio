class Warrior:
    health = 50
    attack = 5
    pass

    @property
    def is_alive(self) -> bool:
        return self.health > 0


class Knight(Warrior):
    attack = 7
    pass


def fight(unit_1: Warrior, unit_2: Warrior) -> bool:
    while unit_1.is_alive and unit_2.is_alive:
        unit_2.health -= unit_1.attack
        unit_1.health -= unit_2.attack if unit_2.is_alive else 0
    return unit_1.is_alive or unit_1.is_alive == unit_2.is_alive
