def safe_pawns(pawns: set) -> int:
    # First: Create board 8 x 8
    board = [[0 for _ in range(8)] for _ in range(8)]
    # Second: Create a files set
    files = "abcdefgh"
    # Third: Create result set
    saved_pawns = set()
    for pawn_id in pawns:
        # Fourth: mark a pawns on the board
        board[int(pawn_id[1]) - 1][files.index(pawn_id[0])] = 1
    # Fifth: go thought each cell on the board expect last range
    for rank in range(7):
        for file in range(8):
            # If pawn exist on current cell then check there are exist other pawns on tje diagonal cells
            # If exist put a tuple with rank and file location to the result set
            if file != 0 and board[rank][file] and board[rank + 1][file - 1]:
                    saved_pawns.add((rank, file - 1))
            if file != 7 and board[rank][file] and board[rank + 1][file + 1]:
                    saved_pawns.add((rank, file + 1))
    # And now return the length of result set
    return len(saved_pawns)