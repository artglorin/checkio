def long_repeat(line):
    """
        length the longest substring that consists of the same char
    """
    letter = ""
    count = 0
    result = 0
    for i in range(len(line)):
        if line[i] == letter:
            count += 1
        else:
            result = max(count, result)
            count = 1
            letter = line[i]
    result = max(count, result)
    return result


if __name__ == '__main__':
    assert long_repeat('sdsffffse') == 4, "First"
    assert long_repeat('ddvvrwwwrggg') == 3, "Second"
    assert long_repeat('abababaab') == 2, "Third"
    assert long_repeat('') == 0, "Empty"
    print('"Run" is good. How is "Check"?')