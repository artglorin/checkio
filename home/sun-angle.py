def sun_angle(time):
    # First: split by :
    # Second: subtract 6 hours from hours
    # Third: convert time to the minutes
    # Fourth:
    # if time between zero and twelve hours in minutes then divide time by four
    # It Is relation of
    # 12 * 60    hours * 60 + minutes       4
    # ------- = --------------------- === ----
    #   180             x                   1
    # else return "I don't see the sun!"
    hours, minutes = (float(x) for x in time.split(":"))
    hours -= 6
    time_in_minutes = hours * 60 + minutes
    if 0 <= time_in_minutes <= (12 * 60):
        return time_in_minutes / 4
    else:
        return "I don't see the sun!"
