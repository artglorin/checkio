import string


def to_encrypt(text, delta):
    # First: go through the letters in a text
    # Second: collect letters to the list
    # Third: leave letter as is if it is a space
    # Fourth:  another case get a unicode of letter, add the delta
    #          and get  a char from alphabet from position by result divided by module of alphabet length
    # Fifth: join result
    return "".join([letter
                    if letter.isspace()
                    else string.ascii_lowercase[(ord(letter) - (ord('a')) + delta) % len(string.ascii_lowercase)]
                    for letter in text])