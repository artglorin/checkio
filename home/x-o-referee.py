from typing import List


def findWinner(matrix):
    for line in matrix:
        if line.count(line[0]) == 3 and line[0] != ".":
            return line[0]
    return None


def checkio(game_result: List[str]) -> str:
    # First: searching winner in rows
    # Second: Transpose matrix and do searching there
    # Third: creating two lists with value from diagonal with enumerate and generator and do searching there
    # Fourth: return "D" if we not found winner
    return findWinner(game_result) \
           or findWinner([*zip(*game_result)]) \
           or findWinner(
        [[game_result[x][y] for x, y in enumerate(range(3))], [game_result[2 - x][y] for x, y in enumerate(range(3))]]
    ) \
           or "D"
