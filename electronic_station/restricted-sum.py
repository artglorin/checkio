def checkio(data):
    """
    # The convention of map function talk about:
    # The function from parameter will be accept on each element in iterable object.
    #
    # It is what we want.
    # But we still need to somehow run our map function.
    # Ok. Let's create a new list and class which will be return the result of operations on it.
    # The last element in the result list will be result of add operation on all previous items

    :param data:
    :return:
    """
    class Computer:
        result = 0
        pass

        def add(self, num: int) -> int:
            self.result += num
            return self.result
    result = Computer()
    return list(map(lambda x: result.add(x), data))[-1]


if __name__ == '__main__':
    assert checkio([1, 2, 3]) == 6, "First example"
    assert checkio([1, 2]) == 3, "Second example"
    assert checkio([16, 15]) == 31, "Third example"