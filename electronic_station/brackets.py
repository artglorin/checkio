def checkio(expression: str) -> bool:
    # It is a classic task with stack :)
    # First: create brackets map
    # Second: go through expression string
    # if letter in the key set in brackets map push it into the stack
    # if letter in the value set in brackets map
    # then get value from bracket map by key from top of stack item with current letter.
    # if equals go next else return false.
    # In the end of cycle stack must be empty
    brackets_map = {"{": "}", "(": ")", "[": "]"}
    stack = []

    for letter in expression:
        if letter in brackets_map.keys():
            stack.append(letter)
        elif letter in brackets_map.values():
            if len(stack) == 0:
                return False
            elif brackets_map[stack.pop()] != letter:
                return False
    return len(stack) == 0