# How can i detect a palindromic?
# I found two way.
# 1. If equaled letters near
# 2. If some other letter between equaled letters
# Ok.
# If I detect palindromic then I should go to the opposite ways by indexes till letter by indexes are equals.
# Then I should cut the text between indexes.This is the sought palindrome.
# And of course I should to look after indexes boundary.
# I think in average Big O notation of this algorithm O(n), without recursion and big complex structures


def longest_palindromic(text):
    length = len(text)
    result = text[0]
    for i in range(length - 1):
        longest_palindromic_ = get_longest_string(is_palindromic(i, i + 1, text), is_palindromic(i, i + 2, text))
        result = get_longest_string(result, longest_palindromic_)
    return result


def get_longest_string(first_string: str, second: str) -> str:
    return first_string if len(first_string) >= len(second) else second


def is_palindromic(left_index: int, right_index: int, text: str) -> str:
    match_size = False
    while is_equals_letter(left_index, right_index, text):
        match_size = True
        left_index -= 1
        right_index += 1
    return text[left_index + 1: right_index] if match_size else ""


def is_equals_letter(left_index: int, right_index: int, text: str) -> bool:
    length = len(text)
    return left_index < right_index \
       and is_correct_left_index(left_index, length) \
       and is_correct_right_index(right_index, length) \
       and text[left_index] == text[right_index]


def is_correct_right_index(right_index: int, length: int) -> bool:
    return right_index < length


def is_correct_left_index(left_index: int, length: int):
    return 0 <= left_index < length - 2