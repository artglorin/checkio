def checkio(n, m):
    return f'{n^m:b}'.count('1')