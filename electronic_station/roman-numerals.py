def checkio(data):
    # I've been thinking about how best to write an algorithm.
    # However, I came to the conclusion that describing the value of each order in the list
    # and making a recursive call calculating from the largest order
    # is the simplest and most efficient method
    return resolve(3, [
        (1, ["I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX"]),
        (10, ["X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC"]),
        (100, ["C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM"]),
        (1000, ["M", "MM", "MMM"])
    ], data)


def resolve(index: int, char_list: list, number: int) -> str:
    if index < 0:
        return ""
    value, lst = char_list[index]
    count = number // value
    return (lst[count - 1] if count != 0 else "") + resolve(index - 1, char_list, number - count * value)
