values = {'I': 1, 'V': 5, 'X': 10, 'L': 50, 'C': 100, 'D': 500, 'M': 1000}


def reverse_roman(roman_string: str):
    result = 0
    for letter in range(len(roman_string) - 1):
        current_value = values[roman_string[letter]]
        next_value = values[roman_string[letter + 1]]
        result += -current_value if current_value < next_value else current_value
    result += values[roman_string[-1]]
    return result