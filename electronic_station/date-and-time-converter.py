
from datetime import datetime


def date_time(time: str) -> str:
    dt = datetime.strptime(time, "%d.%m.%Y %H:%M")
    return "{} {} year {} {} {} {}".format(dt.day, dt.strftime("%B %Y"),
                                                   dt.hour, "hour" if dt.hour == 1 else "hours",
                                                   dt.minute, "minute" if dt.minute == 1 else "minutes")
