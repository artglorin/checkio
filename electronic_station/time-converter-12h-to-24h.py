import time as tm


def time_converter(time):
    return tm.strftime("%H:%M", tm.strptime(time.replace(".", "").upper(), "%I:%M %p"))

