
def largest_histogram(histogram):

    # This is dictionary where stored accumulative temporary values
    bar_height_count_map: dict = {size: 0 for size in set(histogram)}
    # This is all height of bars in the chart
    bar_heights: list = sorted(bar_height_count_map.keys())
    # This is immutable value, put it to the variable for best performance
    bar_height_size: int = len(bar_heights)
    # This is the result of algorithm
    max_rectangle_area = 0
    # Go through chart bars step by step
    for bar in histogram:
        # get index of current bar size from the bar_heights
        bar_index = bar_heights.index(bar)
        if bar_index < bar_height_size:
            # if it is not the tallest bar check records about taller bars for flush area to the result
            for key in bar_heights[bar_index + 1::]:
                bar_height_count = bar_height_count_map[key]
                # If count == then 0 break loop, because all other values must be flushed
                if bar_height_count == 0:
                    break
                # flush the area to the result
                max_rectangle_area = max(max_rectangle_area, key * bar_height_count)
                # reset counter
                bar_height_count_map[key] = 0
        # go through the slice from begin until the current size and increment all counters by one
        for i in bar_heights[:bar_index + 1:]:
            bar_height_count_map[i] += 1
    # Flush all values to the result
    for key, value in bar_height_count_map.items():
        if value == 0:
            break
        max_rectangle_area = max(max_rectangle_area, key * value)
    return max_rectangle_area