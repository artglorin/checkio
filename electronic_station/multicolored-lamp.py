import enum


class LightsColors(enum.Enum):
    GREEN = 0
    RED = 1
    BLUE = 2
    YELLOW = 3
    pass

    def __init__(self, order: int) -> None:
        super().__init__()
        self.order = order

    def __str__(self):
        return self.name.title()


class LightsState:

    def __init__(self) -> None:
        super().__init__()
        self.color = LightsColors.GREEN

    def change_state(self):
        if self.color == LightsColors.GREEN:
            result = LightsColors.RED
        elif self.color == LightsColors.RED:
            result = LightsColors.BLUE
        elif self.color == LightsColors.BLUE:
            result = LightsColors.YELLOW
        else:
            result = LightsColors.GREEN
        self.color = result


class Lamp:

    def __init__(self) -> None:
        super().__init__()
        self.state = LightsState()

    def light(self) -> str:
        result = str(self.state.color)
        self.state.change_state()
        return result